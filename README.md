# Vorlage für Dokumentation
## mit Markdown

Auf Grundlage von: https://quarto.org/

## Installation und Nutzung von Quarto:  
https://quarto.org/docs/get-started/

## Nach der Installation

Für den PDF Export ist ein TeX-Distribution nötig:  
https://quarto.org/docs/output-formats/pdf-basics.html

```terminal
quarto install tinytex
```

### Systemschriften finden
https://quarto.org/docs/output-formats/pdf-basics.html#unicode-characters

```terminal
fc-list :lang=<lang>
```
---

### Erstellung der PDF

```terminal
quarto render
```

Die fertige PDF befindet sich danach im "_output" Ordner

---

### Empfohlene Addons für VSCode

- Quarto
- Markdown All in One
- Markdown Table